/*
 * LinkedListIterator.h
 *
 *  Created on: Mar 2, 2016
 *      Author: bkubiak
 */

#ifndef LINKEDLISTITERATOR_H_
#define LINKEDLISTITERATOR_H_

#include "Node.h"

template<class T>
class LinkedListIterator {
private:
	Node<T>* currentNode;
public:
	LinkedListIterator(Node<T>* firstNode) {
		currentNode = firstNode;
	}

	bool hasNext() {
		return (currentNode != NULL);
	}

	T* next() {
		T* element = NULL;
		if (hasNext()) {
			element = currentNode->getElement();
			currentNode = currentNode->getNext();
		}
		return element;
	}
};

#endif /* LINKEDLISTITERATOR_H_ */
