#include <iostream>
using namespace std;
#include "gtest/gtest.h"
#include "../src/LinkedBST.h"
#include "../src/LinkedList.h"

TEST(balanced, checkingBalanceof5ElmBST){
	int arr[] = {5, 2, 1, 6, 3};

	LinkedBST<int>* list = new LinkedBST<int>();
	for(int i = 0; i < 5; i++){
		int* elem = new int(arr[i]);
		EXPECT_TRUE(list->add(elem));
	}

	EXPECT_TRUE(list->isBalanced());

	delete list;
}

TEST(balanced, checkingBalanceof0ElmBST){
	LinkedBST<int>* list = new LinkedBST<int>();

	//Add empty elm
	EXPECT_FALSE(list->add(NULL));

	EXPECT_TRUE(list->isBalanced());

	delete list;
}

TEST(balanced, checkingBalanceof3ElmBST){
	int arr[] = {5, 2, 1};
	LinkedBST<int>* list = new LinkedBST<int>();

	for(int i = 0; i < 3; i++){
		int* elem = new int(arr[i]);

		EXPECT_TRUE(list->add(elem));
	}

	EXPECT_FALSE(list->isBalanced());

	delete list;
}

TEST(balanced, checkingBalanceof1ElmBST){
	int arr[] = {3};

	LinkedBST<int>* list = new LinkedBST<int>();

	for(int i = 0; i < 1; i++){
		int* elem = new int(arr[i]);

		EXPECT_TRUE(list->add(elem));
	}

	EXPECT_TRUE(list->isBalanced());

	delete list;
}

TEST(diameter, getDiameterof5ElmBST){
	int arr[] = {5, 2, 1, 6, 3};

	LinkedBST<int>* list = new LinkedBST<int>();
	for(int i = 0; i < 5; i++){
		int* elem = new int(arr[i]);
		EXPECT_TRUE(list->add(elem));
	}

	EXPECT_EQ(4, list->getDiameter());
	delete list;
}

TEST(diameter, getDiameterof0ElmBST){
	LinkedBST<int>* list = new LinkedBST<int>();

	EXPECT_FALSE(list->add(NULL));

	EXPECT_EQ(0, list->getDiameter());

	delete list;
}

TEST(diameter, getDiameterof1ElmBST){
	int arr[] = {3};

	LinkedBST<int>* list = new LinkedBST<int>();
	for(int i = 0; i < 1; i++){
		int* elem = new int(arr[i]);
		EXPECT_TRUE(list->add(elem));
	}

	EXPECT_EQ(1, list->getDiameter());

	delete list;
}

TEST(diameter, getDiameterof3ElmBST){
	int arr[] = {5, 2, 1};

	LinkedBST<int>* list = new LinkedBST<int>();
	for(int i = 0; i < 3; i++){
		int* elem = new int(arr[i]);
		EXPECT_TRUE(list->add(elem));
	}

	EXPECT_EQ(3, list->getDiameter());

	delete list;
}

// TEST(completeBST, createCompleteBSTFrom5ElmBST){
// 	int arr[] = {5, 2, 1, 6, 3};

// 	LinkedBST<int>* list = new LinkedBST<int>();
// 	for(int i = 0; i < 5; i++){
// 		int* elem = new int(arr[i]);
// 		EXPECT_TRUE(list->add(elem));
// 	}

// 	LinkedBST<int>* completeBST = list->completeBST();

// 	int completeBSTPreorderArr[] = {5, 2, 1, 3, 6};

// 	LinkedList<int>* completeBSTPreorder = completeBST->preorder();

// 	for(int i = 0; i < completeBSTPreorder->size(); i++){
// 		EXPECT_EQ(completeBSTPreorderArr[i], *completeBSTPreorder->get(i));
// 	}

// 	delete list;
// 	delete completeBST;
// 	delete completeBSTPreorder;
// }
int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

