#include <iostream>

using namespace std;

void writeLine(char c, int n){
    if(n == 0)
        return;

    cout << c;
    writeLine(c, n-1);
}
void writeBlock(char c, int n, int r){
    if(r == 0)
        return;

    writeLine(c, n);
    cout << endl;
    writeBlock(c, n, r-1);
}

int getValue(int a, int b, int n){
    int returnValue = 0;

    cout << "Enter: a=" << a << " b= " << b << endl;
    int c = (a + b)/2;
    if(c*c <= n)
        returnValue = c;
    else
        returnValue = getValue(a, c-1, n);

    cout << "Leave: a= " << a << " b= " << b << endl;
    return returnValue;
}

int search(int first, int last, int n){
    int returnValue = 0;
    cout << "Enter: first = " << first << " last = "
        << last << endl;

    int mid = (first + last)/2;
    if( (mid*mid <= n) && (n < (mid+1) * (mid+1)))
        returnValue = mid;
    else if(mid *mid > n)
        returnValue = search(first, mid-1, n);
    else
        returnValue = search(mid+1, last, n);

    cout << "Leave: first = " << first << " last = "
        << last << endl;

    return returnValue;
}

int mystery(int n){
    return search(1, n, n);
}

void displayOctal(int n){
    if(n > 0)
    {

        if (n/8 >0) {

            displayOctal(n / 8);
        }
        cout << "N is: " << n << endl;
        cout << n % 8;

    }
    cin.get();
}

int f(int n){
    cout << "Function entered with n = " << n << endl;
    switch(n){
        case 0: case 1: case 2:
            return n + 1;
        default:
            return f(n-2) * f(n-4);
    }
}
int main(){
    writeBlock('*', 5, 3);
    int value = getValue(1, 7, 7);
    cout << "Value= " << value << endl;

    cout << "Mystery and Search " << endl;
    cout << mystery(30) << endl;

    cout << "Display Octal" << endl;
    displayOctal(100);

    cout << "F function call" << endl;
    cout << "The value of f(-10000  ) is " << f(-10000) << endl;

    return 0;
}