/*
 * LinkedList.h
 *
 *  Created on: Feb 26, 2016
 *      Author: bkubiak
 */

#ifndef LINKEDLIST_H_
#define LINKEDLIST_H_

#include <iostream>
using namespace std;
#include "Node.h"

template<class T>
class LinkedList{
private:
	Node<T>* head;
	int length;
	Node<T>* getNode(int position);
public:
	LinkedList();
	virtual ~LinkedList();
	int size();
	bool add(int position, T* element);
	T* remove(int position);
	T* get(int position);
	bool set(int position, T* element);
	bool merge(LinkedList<T>* list);
	void rotate(int k);
	void reverse();
};

template<class T>
LinkedList<T>::LinkedList() {
	head = NULL;
	length = 0;
}

template<class T>
LinkedList<T>::~LinkedList() {
	Node<T>* nodeForRemoval = head;
	while (nodeForRemoval != NULL) {
		head = head->getNext();
		nodeForRemoval->setNext(NULL);
		delete nodeForRemoval;
		nodeForRemoval = NULL;
		nodeForRemoval = head;
	}
}

template<class T>
Node<T>* LinkedList<T>::getNode(int position) {
	Node<T>* node = head;
	for (int var = 0; var < position; ++var) {
		node = node->getNext();
	}
	return node;
}

template<class T>
int LinkedList<T>::size() {
	return length;
}

template<class T>
bool LinkedList<T>::add(int position, T* element) {
	if (element == NULL) {
		return false;
	}
	bool notValidPosition = (position < 0 || position > length);
	if (notValidPosition) {
		return false;
	}
	Node<T>* newNode = new Node<T>(element);
	if (position == 0) {
		newNode->setNext(head);
		head = newNode;
	} else {
		Node<T>* precedingNode = getNode(position - 1);
		newNode->setNext(precedingNode->getNext());
		precedingNode->setNext(newNode);
	}
	++length;
	return true;
}

template<class T>
T* LinkedList<T>::remove(int position) {
	bool notValidPosition = (position < 0 || position >= length);
	if (notValidPosition) {
		return NULL;
	}
	Node<T>* nodeForRemoval = NULL;
	if (position == 0) {
		nodeForRemoval = head;
		head = head->getNext();
	} else {
		Node<T>* precedingNode = getNode(position - 1);
		nodeForRemoval = precedingNode->getNext();
		precedingNode->setNext(nodeForRemoval->getNext());
	}
	T* removedElement = nodeForRemoval->getElement();
	nodeForRemoval->setNext(NULL);

	delete nodeForRemoval;
	nodeForRemoval = NULL;
	--length;
	return removedElement;
}

template<class T>
T* LinkedList<T>::get(int position) {
	bool notValidPosition = (position < 0 || position >= length);
	if (notValidPosition) {
		return NULL;
	}
	Node<T>* node = getNode(position);
	return node->getElement();
}

template<class T>
bool LinkedList<T>::set(int position, T* element) {
	if (element == NULL) {
		return false;
	}
	bool notValidPosition = (position < 0 || position >= length);
	if (notValidPosition) {
		return false;
	}
	Node<T>* node = getNode(position);
	node->setElement(element);
	return true;
}

template <class T>
bool LinkedList<T>::merge(LinkedList<T>* list){
	if(list == NULL || list->size() == 0)
		return false;

	int listTwoSize = list->size();
	int count = 0;
	int placement = 1;

	for(int i = 1; i < length; i++){
		add(placement, list->get(count));
		placement += 2;
		count++;
	}

	for(int i = count; i < listTwoSize; i++)
		add(length, list->get(i));

	return true;
}

template <class T>
void LinkedList<T>::rotate(int k){
	if(k < 0 || k > length){
		return;
	}

	LinkedList<T>* newList  = new LinkedList<T>();
	int placement = k;

	for(int i = 0; i < length; i++){
		if(placement == length)
			placement = 0;

		newList->add(newList->size(), get(placement));
		placement++;
	}
	for(int i = 0; i < length; i++)
		set(i, newList->get(i));

	for(int i = 0; i < newList->size(); i++) {
		T *element = new T();
		newList->set(i, element);
	}

	delete newList;
}

template <class T>
void LinkedList<T>::reverse(){
	if (length == 0)
		return;

	LinkedList<T>* newList = new LinkedList<T>();
	int count = 0;

	for(int i = length; i > 0; i--){
		newList->add(count, get(i-1));
		count++;
	}

	for(int i = 0; i < length; i++)
		set(i, newList->get(i));

	for(int i = 0; i < length; i++){
		T* element = new T();
		newList->set(i, element);
	}

	delete newList;
}

#endif /* LINKEDLIST_H_ */
