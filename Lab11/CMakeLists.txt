cmake_minimum_required(VERSION 2.8)

project(Lab11)

enable_testing()

add_subdirectory(src)
add_subdirectory(test)