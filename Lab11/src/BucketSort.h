//
// Created by seone on 4/5/2016.
//

#ifndef LAB11_CLION_BUBBLESORT_H
#define LAB11_CLION_BUBBLESORT_H

class BucketSort {
private:
    int* elements;
    unsigned int size;

    void execute(){
        bucketSort(elements, size);
    }

    void bucketSort(int arr[], int n){
        int count[100];

        for(int i = 0; i < 100; i++)
            count[i] = 0;

        for(int i = 0; i < n; i++)
            count[arr[i]] += 1;

        int sortedArray[n];

        for(int i = 0, j = 0; j < 100; j++){
            for(int k = count[j]; k > 0; k--)
                sortedArray[i++] = j;
        }

        elements = sortedArray;
    }

public:
    BucketSort(int* oldArray, int size) : elements(oldArray), size(size){
        execute();
    }

    ~BucketSort(){
        delete elements;

        size = 0;
    }

    int* returnBucketSorted(){
        if(size == 0)
            return NULL;

        return elements;
    }

    unsigned int getSize(){return size;}
};
#endif //LAB11_CLION_BUBBLESORT_H
