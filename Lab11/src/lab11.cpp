//============================================================================
// Name        : lab11.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

#include "Person.h"
#include "QuickSort.h"
#include "BucketSort.h"
#include "SortDescending.h"

int main() {
	string lastName1 = "Brown";
	string password1 = "Brown123";
	string lastName2 = "Blue";
	string password2 = "Blue123";
	string lastName3 = "Blue";
	string password3 = "Black123";
	Person p1(lastName1, password1), p2(lastName2, password2), p3(lastName3,
			password3);
	if (p2 < p3) {
		cout << p2.getLastName() + " " + p2.getPassword();
	} else
		cout << p3.getLastName() + " " + p3.getPassword();

	cout << "!!!Hello World!!!" << endl; // prints !!!Hello World!!!

	int arr[] = {5, 29, 12, 1, 2};
	int arr2[] = {2, 3, 5, 7, 8, 9, 1, 3, 2};

	QuickSort *newSort = new QuickSort(arr, 5);

	int secondSmallest = newSort->getPlacement(2);

	cout << secondSmallest << endl;

	BucketSort* newBucketSort = new BucketSort(arr2, 9);

	int* bucketSorted = newBucketSort->returnBucketSorted();
	int size = newBucketSort->getSize();

	for(int i = 0; i < size; i++) {
		cout << *bucketSorted << " ";
		bucketSorted++;
	}
	cout << endl << endl;

	int arr3[] = {1, 3, 4, 1, 4, 3, 2, 3, 3, 4, 5};
	SortDescending* sortDesc = new SortDescending(arr3, 11);

	int* descSort = sortDesc->sortedArray();
	size = sortDesc->getSize();

	for(int i = 0; i < size; i++){
		cout << *descSort << " ";
		descSort++;
	}

	delete sortDesc;
	delete bucketSorted;
	delete newSort;
	return 0;
}
