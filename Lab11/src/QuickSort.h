//
// Created by seone on 4/5/2016.
//

#ifndef LAB11_CLION_QUICKSORT_H
#define LAB11_CLION_QUICKSORT_H

class QuickSort {
private:
    int* elements;
    unsigned int size;

    void execute(){
        quickSort(elements, 0, size-1);
    }

    void quickSort(int arr[], int left, int right){
        int i = left, j = right;
        int tmp;
        int pivot = arr[(left + right) /2];

        while(i <= j){
            while(arr[i] < pivot)
                i++;
            while(arr[j] > pivot)
                j--;
            if(j <= j){
                tmp = arr[i];
                arr[i] = arr[j];
                arr[j] = tmp;
                i++;
                j--;
            }
        }

        if(left < j)
            quickSort(arr, left, j);
        if( i < right)
            quickSort(arr, i, right);
    }
public:
    QuickSort(int* oldArray, int size) : elements(oldArray), size(size){
        execute();
    }

    ~QuickSort(){
        delete elements;
        size = 0;
    }
    int getPlacement(int pos){
    	if(pos > size || pos < 0)
    		return -1;

        int p = 0;
        while(p < (pos-1)){
            elements++;
            p++;
        }
        return *elements;
    }

    unsigned int getSize(){return size;}

};
#endif //LAB11_CLION_QUICKSORT_H
