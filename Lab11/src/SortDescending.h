//
// Created by seone on 4/5/2016.
//

#ifndef LAB11_CLION_SORTDESCENDING_H
#define LAB11_CLION_SORTDESCENDING_H

class SortDescending {
private:
    int* elements;
    unsigned int size;

    void execute(){
        int count[1000];

        for(int i = 0; i < 1000; i++){
            count[i] = 0;
        }

        for(int i = 0; i < size; i++){
            count[elements[i]] += 1;
        }

        int* sortedDescArray = new int[size];
        for(int i = 0; i < size; i++){
            sortedDescArray[i] = 0;
        }

        int least = 1;
        for(int i = 999, l = 0; i >= 0; i--){
            for(int j = 999; j >= 0; j--){
            	if(count[j] == least){
            		for(int k = 0; k < least; k++)
            			sortedDescArray[l++] = j;
                }
            }
            least++;
        }

        for(int i = 0, j = size; j > 0; j--, i++){
            elements[i] = sortedDescArray[j-1];
        }
    }

public:
    SortDescending(int oldArray[], int size): size(size){
        elements = new int[size];

        for(int i = 0; i < size; i++){
            elements[i] = oldArray[i];
        }
        execute();
    }

    ~SortDescending(){
        delete elements;

        size = 0;
    }

    int* sortedArray(){
    	if(size == 0)
    		return NULL;
    	return elements;}

    int getSize(){return size;}
};
#endif //LAB11_CLION_SORTDESCENDING_H
