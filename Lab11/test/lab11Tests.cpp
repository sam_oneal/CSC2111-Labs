#include <iostream>
using namespace std;
#include "gtest/gtest.h"
#include "../src/QuickSort.h"
#include "../src/BucketSort.h"
#include "../src/SortDescending.h"

TEST(QuickSort, gettingLowestElmFromQuickSort){
	//Values
	int arr[] = {3, 19, 2, 1, 9, 23};
	int size = 6;
	QuickSort *quickSort = new QuickSort(arr, size);

	//Lowest value
	int lowest = quickSort->getPlacement(1);

	EXPECT_EQ(lowest, 1);

	delete quickSort;
}

TEST(QuickSort, gettingHighestElmFromQuickSort){
	//Values
	int arr[] = {4, 28, 19, 2, 2, 10, 200};
	int size = 7;
	QuickSort *quickSort = new QuickSort(arr, size);

	//Highest Value
	int highest = quickSort->getPlacement(size);

	EXPECT_EQ(highest, 200);

	delete quickSort;
}

TEST(QuickSort, gettingLowestElmFromEmptyArr){
	//Values
	int arr[] = {};
	int size = 0;
	QuickSort* quickSort = new QuickSort(arr, size);

	//Lowest Value
	int lowest = quickSort->getPlacement(1);

	EXPECT_EQ(lowest, -1);

	delete quickSort;
}

TEST(BucketSort, sortingBucketWith8Elms){
	//Values
	int arr[] = {3, 29, 19, 2, 1, 9, 7, 8};
	int size = 8;
	BucketSort* bucketSort = new BucketSort(arr, size);

	//Returned value
	int* sortedArray = bucketSort->returnBucketSorted();
	//Expected Values
	int expectArray[] = {1, 2, 3, 7, 8, 9, 19, 29};

	for(int i = 0; i < size; i++){
		EXPECT_EQ(*sortedArray, expectArray[i]);
		sortedArray++;
	}

	delete bucketSort;
}

TEST(BucketSort, sortingBucketWith1Elm){
	//Values
	int arr[] = {7};
	int size = 1;
	BucketSort* bucketSort = new BucketSort(arr, size);

	//Returned Value
	int* sortedArray = bucketSort->returnBucketSorted();
	//Expected Values
	int expectedArray[] = {7};

	for(int i = 0; i < size; i++){
		EXPECT_EQ(*sortedArray, expectedArray[i]);
		sortedArray++;
	}

	delete bucketSort;
}

TEST(BucketSort, sortingBucketWithNoElms){
	//Values
	int arr[] = {};
	int size = 0;
	BucketSort* bucketSort = new BucketSort(arr, size);

	//Returned value
	int* sortedArray = bucketSort->returnBucketSorted();
	//Expected Values
	bool isEmpty;
	if(sortedArray == NULL)
		isEmpty = true;
	else
		isEmpty = false;

	EXPECT_TRUE(isEmpty);

	delete bucketSort;
}

TEST(SortDescending, sortingDescendingWith8Elms){
	//Values
	int arr[] = {1, 3, 4, 1, 4, 3, 2, 3, 3, 4, 5};
	int size = 11;
	SortDescending* sortDesc = new SortDescending(arr, size);

	//Returned value
	int* descSort = sortDesc->sortedArray();
	//Expected Values
	int sortedDesc[] = {3, 3, 3, 3, 4, 4, 4, 1, 1, 5, 2};

	for(int i = 0; i < size; i++){
		EXPECT_EQ(*descSort, sortedDesc[i]);
		descSort++;
	}

	delete sortDesc;
}

TEST(SortDescending, sortingDescendingWith2Elms){
	//Values
	int arr[] = {2, 5};
	int size = 2;
	SortDescending* sortDesc = new SortDescending(arr, size);

	//Returned Values
	int* descSort = sortDesc->sortedArray();
	//Expected values
	int sortedDesc[] = {5, 2};

	for(int i = 0; i < size; i++){
		EXPECT_EQ(*descSort, sortedDesc[i]);
		descSort++;
	}

	delete sortDesc;
}

TEST(SortDescending, sortingDescendingWithNoElms){
	//Values
	int arr[] = {};
	int size = 0;

	SortDescending* sortDesc = new SortDescending(arr, size);

	//Returned Values
	int* descSort = sortDesc->sortedArray();
	//Expect Values
	bool isEmpty;
	if(descSort == NULL)
		isEmpty = true;
	else
		isEmpty = false;

	EXPECT_TRUE(isEmpty);

	delete sortDesc;
}

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

