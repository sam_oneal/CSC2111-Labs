find_package(Threads REQUIRED)

#Enable ExternalProject CMake module
include(ExternalProject)

#Download and install GoogleTest
ExternalProject_Add(
	gtest
	URL https://googletest.googlecode.com/files/gtest-1.7.0.zip
	PREFIX ${CMAKE_CURRENT_BINARY_DIR}/gtest
	#Diable install step
	INSTALL_COMMAND ""
)

#Create a libgtest target to be used as a dependecy by test programs
add_library(libgtest IMPORTED STATIC GLOBAL)
add_dependencies(libgtest gtest)

#Set gtest properties
ExternalProject_Get_Property(gtest source_dir binary_dir)
set_target_properties(libgtest PROPERTIES
	"IMPORTED_LOCATION" "${binary_dir}/libgtest.a"
	"IMPORTED_LINK_INTERFACE_DIRECTORIES" "${CMAKE_THREAD_LIBS_INIT}"
#	"INTERFACE_INCLUDE_DIRECTORIES"	"${source_dir}/include"
)

#INTERFACE_INCLUDE_DIRECTOIRES does not work correctly
include_directories("${source_dir}/include")

set (SRC lab11Tests.cpp)

ADD_EXECUTABLE(lab11test ${SRC})

TARGET_LINK_LIBRARIES(
	libgtest
)

install(TARGETS lab11test DESTINATION Unit)

add_test(NAME lab11Test
	COMMAND lab11Test)