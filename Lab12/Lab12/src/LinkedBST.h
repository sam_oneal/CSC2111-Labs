/*
 * LinkedBST.h
 *
 *  Created on: Apr 6, 2015
 *      Author: Beata
 */

#ifndef LINKEDBST_H_
#define LINKEDBST_H_

#include "BinarySearchTree.h"
#include "BTNode.h"
#include "LinkedList.h"

template<class T>
class LinkedBST: public BinarySearchTree<T> {
private:
	BTNode<T>* root;

	void destroy(BTNode<T>* node);

	BTNode<T>* add(BTNode<T>* currentNode, BTNode<T>* newNode);
	void preorderRec(BTNode<T>*, LinkedList<T>*);
	void postorderRec(BTNode<T>*, LinkedList<T>*);
	void inorderRec(BTNode<T>*, LinkedList<T>*);
	void minimize(LinkedBST<T>*, T[], int, int);
	int calcHeight(BTNode<T>*);

public:
	LinkedBST();
	~LinkedBST();
	bool add(T* element);
	int height();
	LinkedList<T>* preorder();
	LinkedList<T>* postorder();
	LinkedList<T>* inorder();
	LinkedBST<T>* buildMinHeightBST();
};

template<class T>
LinkedBST<T>::LinkedBST() {
	root = NULL;
}

template<class T>
LinkedBST<T>::~LinkedBST() {
	destroy(root);
}

template<class T>
void LinkedBST<T>::destroy(BTNode<T>* node) {
	if (node != NULL) {
		destroy(node->getLeftSubtree());
		destroy(node->getRightSubtree());
		delete node;
		node = NULL;
	}
}

template<class T>
bool LinkedBST<T>::add(T* element) {
	if (element == NULL) {
		return false;
	}

	BTNode<T>* newNode = new BTNode<T>(element);
	root = add(root, newNode);
	return true;
}

template<class T>
BTNode<T>* LinkedBST<T>::add(BTNode<T>* currentNode, BTNode<T>* newNode) {
	if (currentNode == NULL) {
		return newNode;
	} else if (*currentNode->getElement() > *newNode->getElement()) {
		BTNode<T>* temp = add(currentNode->getLeftSubtree(), newNode);
		currentNode->setLeftSubtree(temp);
	} else if (*currentNode->getElement() < *newNode->getElement()) {
		BTNode<T>* temp = add(currentNode->getRightSubtree(), newNode);
		currentNode->setRightSubtree(temp);
	}
	return currentNode;
}

template<class T>
LinkedList<T>* LinkedBST<T>::preorder() {
	if(root == NULL)
		return NULL;

	LinkedList<T>* preorderList = new LinkedList<T>();

	preorderRec(root, preorderList);

	return preorderList;
}

template<class T>
void LinkedBST<T>::preorderRec(BTNode<T> *currentNode, LinkedList<T> *data) {
	if(currentNode != NULL) {
		data->add(data->size(), currentNode->getElement());

		preorderRec(currentNode->getLeftSubtree(), data);

		preorderRec(currentNode->getRightSubtree(), data);
	}
}

template <class T>
LinkedList<T>* LinkedBST<T>::postorder(){
	if(root == NULL)
		return NULL;

	LinkedList<T>* postOrderList = new LinkedList<T>();

	postorderRec(root, postOrderList);

	return postOrderList;
}

template <class T>
void LinkedBST<T>::postorderRec(BTNode<T> *currentNode, LinkedList<T> *data) {
	if(currentNode != NULL){
		postorderRec(currentNode->getLeftSubtree(), data);

		postorderRec(currentNode->getRightSubtree(), data);

		data->add(data->size(), currentNode->getElement());
	}
}

template <class T>
LinkedList<T>* LinkedBST<T>::inorder() {
	if(root == NULL)
		return NULL;

	LinkedList<T>* inorderList = new LinkedList<T>();

	inorderRec(root, inorderList);

	return inorderList;
}

template <class T>
void LinkedBST<T>::inorderRec(BTNode<T>* currentNode, LinkedList<T>* data){
	if(currentNode != NULL){
		inorderRec(currentNode->getLeftSubtree(), data);

		data->add(data->size(), currentNode->getElement());

		inorderRec(currentNode->getRightSubtree(), data);
	}
}

template<class T>
LinkedBST<T>* LinkedBST<T>::buildMinHeightBST() {
	if(root == NULL)
		return NULL;

	LinkedBST<T>* newBST = new LinkedBST<T>();
	LinkedList<T>* inorderList = this->inorder();
	T arr[inorderList->size()];

	for(int i = 0; i < inorderList->size(); i++){
		arr[i] = *inorderList->get(i);
	}

	minimize(newBST, arr, 0, inorderList->size() - 1);
	return newBST;
}

template <class T>
void LinkedBST<T>::minimize(LinkedBST<T>* bst, T arr[], int left, int right) {
	if(left <= right){
		int mid = left + ((right - left)/2);
		T* midElm = new T(arr[mid]);

		bst->add(midElm);

		minimize(bst, arr, 0, mid-1);
		minimize(bst, arr, mid+1, right);
	}
}

template <class T>
int LinkedBST<T>::height(){
	if(root == NULL)
		return 0;

	return calcHeight(root);
}

template <class T>
int LinkedBST<T>::calcHeight(BTNode<T> *currentNode) {
	if(currentNode == NULL)
		return 0;

	int leftHeight = calcHeight(currentNode->getLeftSubtree());
	int rightHeight = calcHeight(currentNode->getRightSubtree());

	return (1 + max(leftHeight, rightHeight));
}
#endif /* LINKEDBST_H_ */
