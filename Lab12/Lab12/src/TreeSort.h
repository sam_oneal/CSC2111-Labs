#include "LinkedBST.h"
#include "LinkedList.h"

class TreeSort {
public:
	static LinkedList<int>* treeSort(int array[], int size){
		if(size == 0)
			return NULL;

		LinkedBST<int>* tree = new LinkedBST<int>();
		for(int i = 0; i < size; i++){
			int* elem = new int(array[i]);
			tree->add(elem);
		}
		LinkedList<int>* list = tree->inorder();
		
		delete tree;

		return list;
	}
};