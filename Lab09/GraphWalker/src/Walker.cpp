/*
 * Walker.cpp
 *
 *  Created on: Mar 22, 2015
 *      Author: bkubiak
 */

#include "Walker.h"
#include "ArrayQueue.h"
#include "ArrayStack.h"
#include "ArrayList.h"

ArrayList<Vertex>* Walker::breadthFirstSearch(Vertex* vertex) {
	if(vertex == NULL)
		return NULL;

	ArrayList<Vertex>* result = new ArrayList<Vertex>();
	ArrayQueue<Vertex>* queue = new ArrayQueue<Vertex>();

	result->add(result->size(), vertex);
	vertex->setVisited(true);
	SortedArrayList<Vertex>* neighbors = vertex->getNeighbors();

	for(int i = 0; i < neighbors->size(); i++){
		if(!neighbors->get(i)->getVisited()) {
			neighbors->get(i)->setVisited(true); //<-- Might work
			queue->enqueue(neighbors->get(i));
		}
	}

	while(!queue->isEmpty()){
		vertex = queue->dequeue();
		vertex->setVisited(true); //<-- Might be duplicate
		result->add(result->size(), vertex);

		neighbors = vertex->getNeighbors();

		for(int i = 0; i < neighbors->size(); i++){

			if(!neighbors->get(i)->getVisited()) {
				neighbors->get(i)->setVisited(true); //<-- Might work
				queue->enqueue(neighbors->get(i));
			}
		}
	}

	delete queue;

	return result;
}

ArrayList<Vertex>* Walker::depthFirstSearch(Vertex* vertex) {
	if(vertex == NULL)
		return NULL;

	ArrayList<Vertex>* result = new ArrayList<Vertex>();
	ArrayStack<Vertex>* stack = new ArrayStack<Vertex>();

	stack->push(vertex);
	result->add(result->size(), vertex);
	vertex->setVisited(true);

	SortedArrayList<Vertex>* neighbors = vertex->getNeighbors();

	Vertex* curr = neighbors->get(0);

	while(!stack->isEmpty()){
		if(!curr->getVisited()){
			stack->push(curr);
			result->add(result->size(), curr);
			neighbors = curr->getNeighbors();
			curr->setVisited(true);
			curr = neighbors->get(0);
		}else{
			bool allVisited = false;
			for(int i = 0; i < neighbors->size(); i++){
				if(!neighbors->get(i)->getVisited()){
					allVisited = false;
					curr = neighbors->get(i);
					break;
				}
				allVisited = true;
			}

			if(allVisited && stack->peek() == vertex){
				stack->pop();
				allVisited = false;
			}
			if(allVisited) {
				stack->pop();
				curr = stack->peek();
				neighbors = curr->getNeighbors();
			}
		}
	}

	delete stack;

	return result;
}
