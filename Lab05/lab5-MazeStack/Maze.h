#if !defined (MAZE_H)
#define MAZE_H

#include "Matrix.h"
using CSC2110::Matrix;
#include "Update.h"
#include "Drawable.h"
#include "ArrayStack.h"
#include "Cell.h"
#include <gtkmm.h>

class Maze : public Drawable
{
   private:
      Matrix* maze;
      Update* gui;

      int width;
      int height;

      bool traverse();
      Cell* processBackTrack(ArrayStack<Cell>* stack);
      void processSolution(ArrayStack<Cell>* stack);
      bool isSolved(Cell* curr_cell, ArrayStack<Cell>* stack);

      int WALL;
      int SPACE;
      int TRIED;
      int BACKTRACK;
      int PATH;

   public:
      Maze(Matrix* maze);
      virtual ~Maze();

      bool solve();
      void addListener(Update* gui);

      virtual void draw(Cairo::RefPtr<Cairo::Context> cr, int width, int height);
      virtual void mouseClicked(int x, int y);

};

#endif
