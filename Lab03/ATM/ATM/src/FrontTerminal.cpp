/*
 * FrontTerminal.cpp
 *
 *  Created on: Jan 25, 2015
 *      Author: B
 */

#include <iostream>
using namespace std;

#include "FrontTerminal.h"
#include "SessionManager.h"
#include "Person.h"

FrontTerminal::FrontTerminal() {
	people = new People();
}

FrontTerminal::~FrontTerminal() {
	//Done:0 delete all dynamically created objects by THIS object
	delete people;
	people = NULL;
}

void FrontTerminal::createAccount() {
	string lastName;
	string password;
	cout << "Last name: ";
	cin >> lastName;
	cout << endl;
	cout << "Password: ";
	cin >> password;
	cout << endl;

	Person* newCustomer = new Person(lastName, password);
	//Done:0 add new customer to the People list
	people->add(newCustomer);
}

void FrontTerminal::logIn() {
	string lastName;
	string password;
	cout << "Last name: ";
	cin >> lastName;
	cout << "Password: ";
	cin >> password;

	Person* loggedInPerson = people->get(lastName, password);
	if (loggedInPerson != NULL) {
		s.setLoggedInPerson(loggedInPerson);
		s.manageAccount();
	} else {
		cout << "Wrong last name or password." << endl;
	}
}
