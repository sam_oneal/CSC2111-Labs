/*
 * FrontTerminal.h
 *
 *  Created on: Jan 25, 2015
 *      Author: B
 */

#ifndef ATM_SRC_FRONTTERMINAL_H_
#define ATM_SRC_FRONTTERMINAL_H_

#include <iostream>
using namespace std;

#include "People.h"
#include "SessionManager.h"

class FrontTerminal {
private:
	SessionManager s;
	People* people;
public:
	FrontTerminal();
	~FrontTerminal();

	void createAccount();
	void logIn();
};

#endif /* ATM_SRC_FRONTTERMINAL_H_ */
