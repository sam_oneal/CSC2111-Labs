/*
 * SessionManager.cpp
 *
 *  Created on: Jan 25, 2015
 *      Author: B
 */

#include <iostream>
using namespace std;

#include "SessionManager.h"

SessionManager::SessionManager() {
	loggedInPerson = NULL;
}

void SessionManager::setLoggedInPerson(Person* newLoggedInPerson) {
	loggedInPerson = newLoggedInPerson;
}

void SessionManager::manageAccount() {
	char option;
	double amount;
	while (true) {
		cout << "If you want to: " << endl;
		cout << "- deposit money press d " << endl;
		cout << "- withdraw press w " << endl;
		cout << "- logout/quit press q" << endl;
		cin >> option;
		if (option == 'd') {
			cout << "How much to deposit? ";
			cin >> amount;
			//Done:0 deposit an amount given in parameter amount to logged-in person
			loggedInPerson->getAccount()->deposit(amount);
		} else if (option == 'w') {
			cout << "How much to withdraw? ";
			cin >> amount;
			bool enoughMoney = loggedInPerson->getAccount()->withdraw(amount);
			if (!enoughMoney) {
				cout << "You don't have enough money in your account." << endl;
			}
		} else if (option == 'q') {
			// terminate the loop
			loggedInPerson = NULL;
			break;
		}
		//Done:0 display data of a logged-in person
		loggedInPerson->displayData();
	}
}
