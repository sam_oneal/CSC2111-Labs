/*
 * SessionManager.h
 *
 *  Created on: Jan 25, 2015
 *      Author: B
 */

#ifndef ATM_SRC_SESSIONMANAGER_H_
#define ATM_SRC_SESSIONMANAGER_H_

#include "Person.h"

class SessionManager {
private:
	Person* loggedInPerson;
public:
	SessionManager();

	void setLoggedInPerson(Person* newLoggedInPerson);

	void manageAccount();
};

#endif /* ATM_SRC_SESSIONMANAGER_H_ */
