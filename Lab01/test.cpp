#include <iostream>

int factorial(unsigned int n){
      if (n == 0)
      return 1;
      
      return n * factorial(n-1);
}
int main() {
      
      std::cout << std::endl << factorial(-1);
      
      return 0;
}